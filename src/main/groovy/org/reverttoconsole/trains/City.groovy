/**
 * 
 */
package org.reverttoconsole.trains;

/**
 * @author Jeff Hemminger
 *
 */
class City {
	
	String name
	int index
	
	public City(String name, int index) {
		this.name = name
		this.index = index
	}
	
	public int getIndex() {
		return this.index
	}
	
	@Override
	public int hashCode() {
		int result = 401575465 + ((name == null) ? 0 : name.hashCode());
		return result;
	}	
	
	@Override
	public boolean equals(Object obj) {
		if(!obj || obj.getClass() != this.getClass()) {
			return false
		}
		City other = (City) obj
		return this.name.equals(other.name)
	}
		
    @Override
	public String toString() {
		return this.name
	}
}
