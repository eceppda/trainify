/**
 * 
 */
package org.reverttoconsole.trains;

import java.util.Stack;
import org.reverttoconsole.trains.engine.RouteMap 

/**
 * @author jeff
 *
 */
class Trip {
	
	Stack<City> cities
	RouteMap map
	
	public Trip(RouteMap map) {
		this.map = map
		this.cities = new Stack<City>()
	}
	
	/**
	 * Regenerate a Trip given some data.
	 * @param map
	 * @param start
	 * @param currentInStack
	 * @param visited
	 */
	public Trip(RouteMap map, City start, City currentInStack, Stack<City> visited) {
		this.map = map
		this.cities = new Stack<City>()		
		int index = visited.lastIndexOf(start)
		boolean hasFoundDest = false
		for(index; index < visited.size(); index++) {
			List<City> list = this.map.getDestinations(currentInStack)
			City c = visited.get(index)
			if((!hasFoundDest && list.contains(c)) || c.equals(start)) {
				this.cities.push c
				if(!c.equals(start)) {
					hasFoundDest = true
				}
			}
		}
	}
	
	void addCity(City city) {
		this.cities.push city
	}
	
	int getNumberOfStops() {
		int i = this.cities.size()
		return  i - 1
	}
	
	List<City> getAsList() {
		return this.cities.toList()
	}
	
	int getDistance() {
		int distance = 0
		List<City> tripList = this.getAsList()
		City prev = null
		for (City city : tripList) {
			if(prev != null) {
				distance += this.map.getDistance(prev,city)
			}           
			prev = city			
		}
		return distance	    
	}
	
	int getDistance(City city) {
		int distance = this.getDistance()
		City last = cities.get(this.cities.size() -1 )
		distance += this.map.getDistance(last,city)
		return distance
	}
	
	@Override
	public String toString() {
		String result = 'Trip List: '
		for(City c in this.cities) {
			result += c.toString()
			result +=  ', '
		}
		return result
	}	
	
}
