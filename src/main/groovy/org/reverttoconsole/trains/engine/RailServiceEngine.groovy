/**
 * 
 */
package org.reverttoconsole.trains.engine


import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;

import org.reverttoconsole.trains.City;
import org.reverttoconsole.trains.Trip 

/**
 * Thanks Renaud Waldura
 * http://renaud.waldura.com/doc/java/dijkstra/
 * 
 * @author Jeff Hemminger
 *
 */
class RailServiceEngine {
	
	public static final int INFINITE_DISTANCE = Integer.MAX_VALUE
	
	private static final int INITIAL_CAPACITY = 9	
	
	private RouteMap map
	
	def shortestDistanceComparator = [ compare: { o1, o2 ->
		getShortestDistance(o1) - getShortestDistance(o2)
	}
	] as Comparator	
	
	private final PriorityQueue<City> unsettledNodes = new PriorityQueue<City>(INITIAL_CAPACITY, shortestDistanceComparator)
	
	private final Set<City> settledNodes = new HashSet<City>()	
	
	private final Map<City, Integer> shortestDistances = new HashMap<City, Integer>()	
	
	private final Map<City, City> predecessors = new HashMap<City, City>()
	
	private boolean startIsDestination = false
	
	
	public RailServiceEngine(RouteMap map) {
		this.map = map
	}
	
	public City getPredecessor(City city) {
		return predecessors.get(city)
	}
	
	/**
	 * Run Dijkstra's shortest path algorithm on the map.
	 */
	public int getShortestDistanceFromTo(City start, City destination) {
		init(start)
		
		// the current node
		City u
		
		while ((u = unsettledNodes.poll()) != null) {
			if(!isSettled(u)) {
				
				if (u == destination) {
					if(start.equals(destination) && settledNodes.size() == 0) {
						this.startIsDestination = true
					} else {
						break
					}
				}
				
				settledNodes.add u 
				
				relaxNeighbors u
			}
		}
		return this.getShortestDistance(destination)
	}   
	
	public List<Trip> tripsFromToWithDelimiter(City start, City goal, def delimiter) {
		Stack<City> nextStack = new Stack<City>();
		Trip currentTrip = new Trip(this.map);
		Stack<City> visited = new Stack<City>()
		
		List<Trip> trips = new ArrayList<Trip>()
		
		//Enqueue root
		nextStack.add start
		
		while (!nextStack.isEmpty()) {
			//Dequeue next node for comparison
			//And add it 2 list of traversed nodes
			City node = nextStack.pop()
			currentTrip.addCity node
			
			visited.push node
			
			if (node.equals(goal) && currentTrip.cities.size() > 1) {
				trips.add currentTrip
				if(!nextStack.empty()) {
					currentTrip = new Trip(this.map, start, nextStack.get(nextStack.size()-1), visited)
				}               
			} else {
				
				//Enqueue new neighbors
				for (City neighbor : map.getDestinations(node)) {
					if(delimiter(currentTrip, neighbor)) {
						nextStack.push(neighbor);
					} else {
						City currentInStack = start
						if(!nextStack.isEmpty()) {
							currentInStack = nextStack.get(nextStack.size()-1)
						}
						currentTrip = new Trip(this.map, start, currentInStack, visited)
					}
				}
			}
		}
		return trips
	}   
	
	private int getShortestDistance(City city) {
		Integer d = shortestDistances.get(city)
		return (d == null) ? INFINITE_DISTANCE : d  
	}   	
	
	private void relaxNeighbors(City u) {
		for (City v : map.getDestinations(u)) {
			
			int shortDist = getShortestDistance(u) + map.getDistance(u, v)
			int nextShort = getShortestDistance(v)
			if (shortDist < nextShort || this.startIsDestination) {
				// assign new shortest distance and mark unsettled
				setShortestDistance(v, shortDist)
				
				// assign predecessor in shortest path
				setPredecessor(v, u)
			}
		}        
	}
	
	private void init(City start) {
		settledNodes.clear()
		unsettledNodes.clear()
		
		shortestDistances.clear()
		predecessors.clear()
		
		setShortestDistance(start,0)
		unsettledNodes.add(start)
	}      
	
	private void setShortestDistance(City city, int distance) {
		unsettledNodes.remove(city)
		shortestDistances.put(city, distance)
		unsettledNodes.add(city)        
	}	
	
	private boolean isSettled(City v) {
		return settledNodes.contains(v)
	}	
	
	private void setPredecessor(City a, City b) {
		predecessors.put(a, b)
	}	
	
}
