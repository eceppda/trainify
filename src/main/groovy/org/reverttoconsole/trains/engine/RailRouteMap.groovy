/**
 * 
 */
package org.reverttoconsole.trains.engine;

import java.util.HashMap;
import java.util.List;

import org.reverttoconsole.trains.City;

/**
 * @author Jeff Hemminger
 *
 */
class RailRouteMap implements RouteMap {
	
	private final int[][] distances
	private HashMap destinationMap
	private HashMap predecessorMap
	
	public RailRouteMap(int cityCount) {
		distances = new int[cityCount][cityCount]
		destinationMap = new HashMap<Integer, City>(cityCount)
		predecessorMap = new HashMap<Integer, City>(cityCount)
	}
	
	public void addDirectRoute(City start, City end, int distance) {
		distances[start.getIndex()][end.getIndex()] = distance
		destinationMap.put end.getIndex(), end
		predecessorMap.put start.getIndex(), start
	}
	
	public int getDistance(City start, City end) {
		return distances[start.getIndex()][end.getIndex()]
	}
	
	public List getDestinations(City city) {
		List<City> list = new ArrayList<City>()
		for( int i = 0; i < distances.length; i++) {
			if(distances[city.getIndex()][i] > 0) {
				list.add(destinationMap.get(i))
			}
			
		}
		return list
	}
	
	public List getPredecessors(City city) {
		List<City> list = new ArrayList<City>()
		
		for(int i=0; i < distances.length; i++) {
			if(distances[i][city.getIndex()] > 0) {
				list.add predecessorMap.get(i)
			}
		}
		return list
	}
	
	public RouteMap getInverse() {
		RailRouteMap transposed = new RailRouteMap(distances.length)
		
		for(int i=0; i < distances.length; i++) {
			for( int j=0; j < distances.length; j++) {
				transposed.distances[i][j] = distances[j][i]
			}
		}
		
		return transposed
	}
}
 