/**
 * 
 */
package org.reverttoconsole.trains.engine;

import java.util.List;

import org.reverttoconsole.trains.City;

/**
 * @author Jeff Hemminger
 *
 */
interface RouteMap {
	
	void addDirectRoute(City start, City end, int distance)
	
	int getDistance(City start, City end)
	
	List getDestinations(City city)
	
	List getPredecessors(City city)
	
	RouteMap getInverse()
	
}
