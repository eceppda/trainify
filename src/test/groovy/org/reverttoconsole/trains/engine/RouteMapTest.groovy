/**
 * 
 */
package org.reverttoconsole.trains.engine;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.reverttoconsole.trains.AbstractTrainTestCase 
import org.reverttoconsole.trains.City;

/**
 * @author jeff
 *
 */
class RouteMapTest extends AbstractTrainTestCase {
	
	
	@Before
	public void setUp() throws Exception {
		initRouteMap()
	}
	
	@Test
	public void getDistances() {		
		assertTrue( railMap.getDistance(a,b) == 5)
		assertTrue( railMap.getDistance(b,c) == 4)
		assertTrue( railMap.getDistance(c,d) == 8)
		assertTrue( railMap.getDistance(d,c) == 8)
		assertTrue( railMap.getDistance(d,e) == 6)
		assertTrue( railMap.getDistance(a,d) == 5)
		assertTrue( railMap.getDistance(c,e) == 2)
		assertTrue( railMap.getDistance(e,b) == 3)
		assertTrue( railMap.getDistance(a,e) == 7)
	}
	
	@Test
	public void getDestinations() {
		List<City> dests = railMap.getDestinations(a)
		assertTrue(dests.size() == 3)
		
		dests = railMap.getDestinations(b)
		assertTrue(dests.size() == 1)
		
		dests = railMap.getDestinations(c)
		assertTrue(dests.size() == 2)
		
		dests = railMap.getDestinations(d)
		assertTrue(dests.size() == 2)
		
		dests = railMap.getDestinations(e)
		assertTrue(dests.size() == 1)		
	}
	
	@Test
	public void getPredecessors() {
		List<City> dests = railMap.getPredecessors(a)
		assertTrue(dests.size() == 0)
		
		dests = railMap.getPredecessors(b)
		assertTrue(dests.size() == 2)
		
		dests = railMap.getPredecessors(c)
		assertTrue(dests.size() == 2)
		
		dests = railMap.getPredecessors(d)
		assertTrue(dests.size() == 2)
		
		dests = railMap.getPredecessors(e)
		assertTrue(dests.size() == 3)
	}
}
