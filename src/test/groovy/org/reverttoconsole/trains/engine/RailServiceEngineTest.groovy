/**
 * 
 */
package org.reverttoconsole.trains.engine;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.reverttoconsole.trains.AbstractTrainTestCase 
import org.reverttoconsole.trains.City;
import org.reverttoconsole.trains.Trip;

/**
 * @author Jeff Hemminger
 *
 */
class RailServiceEngineTest extends AbstractTrainTestCase {
	
	RailServiceEngine engine
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {		
		initRouteMap()
		engine = new RailServiceEngine(railMap)
	}
	
	@Test
	public void numberOfTripToMaxStops() {
		def delimiter = { trip, city ->
			if(trip.cities.size() < 4) {
				return true
			}
			return false
		}
		List<Trip> list = engine.tripsFromToWithDelimiter(c, c, delimiter)
		assertTrue(list.size() == 2)
		Trip t1 = list.get(0)
		Trip t2 = list.get(1)
		List<City> l1 = t1.getAsList()
		List<City> l2 = t2.getAsList()
		assertEquals(4, l1.size())
		assertEquals(3, l2.size())
		assertEquals(c, l1[0])
		assertEquals(e, l1[1])
		assertEquals(b, l1[2])
		assertEquals(c, l1[3])
		assertEquals(c, l2[0])
		assertEquals(d, l2[1])
		assertEquals(c, l2[2])
	}
}
