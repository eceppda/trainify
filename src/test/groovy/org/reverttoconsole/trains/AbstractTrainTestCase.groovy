/**
 * 
 */
package org.reverttoconsole.trains;

import org.reverttoconsole.trains.engine.RailRouteMap 
import org.reverttoconsole.trains.engine.RouteMap 

/**
 * Loads train routes map
 * @author jeff
 *
 */
abstract class AbstractTrainTestCase {
	
	City a = new City('A',0)
	City b = new City('B',1)
	City c = new City('C',2)
	City d = new City('D',3)
	City e = new City('E',4)
	
	RouteMap railMap
	
	public void initRouteMap() {
		railMap = new RailRouteMap(5)
		railMap.addDirectRoute a, b, 5
		railMap.addDirectRoute b, c, 4
		railMap.addDirectRoute c, d, 8
		railMap.addDirectRoute d, c, 8
		railMap.addDirectRoute d, e, 6
		railMap.addDirectRoute a, d, 5
		railMap.addDirectRoute c, e, 2
		railMap.addDirectRoute e, b, 3
		railMap.addDirectRoute a, e, 7
	}
}
