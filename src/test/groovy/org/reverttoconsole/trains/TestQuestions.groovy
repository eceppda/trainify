/**
 * 
 */
package org.reverttoconsole.trains;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test 
import org.reverttoconsole.trains.engine.RailServiceEngine 

/**
 * These are the tests corresponding to the test questions.
 * 
 * @author Jeff Hemminger
 *
 */
class TestQuestions extends AbstractTrainTestCase {
	
	RailServiceEngine engine
	
	@Before
	public void setUp() throws Exception {
		initRouteMap()
		engine = new RailServiceEngine(railMap)
	}
	
	/**
	 * 1. The distance of the route A-B-C.
	 */
	@Test
	public void distanceOfRouteABC() {
		Trip trip = new Trip(railMap)
		trip.addCity a
		trip.addCity b
		trip.addCity c
		def distance = trip.getDistance()
		assertTrue(distance == 9)
		this.generateOutput "1", distance.toString()
	}
	
	/**
	 * 2. The distance of the route A-D.
	 */
	@Test
	public void distanceOfRouteAD() {
		Trip trip = new Trip(railMap)
		trip.addCity a
		trip.addCity d
		def distance = trip.getDistance()
		assertTrue(distance == 5)
		this.generateOutput "2", distance.toString()
	}
	
	/**
	 * 3. The distance of the route A-D-C.
	 */
	@Test
	public void distanceOfRouteADC() {
		Trip trip = new Trip(railMap)
		trip.addCity a
		trip.addCity d
		trip.addCity c
		def distance = trip.getDistance()
		assertTrue(distance == 13)
		this.generateOutput "3", distance.toString()		
	}
	
	/**
	 * 4. The distance of the route A-E-B-C-D.
	 */
	@Test
	public void distanceOfRouteAEBCD() {
		Trip trip = new Trip(railMap)
		trip.addCity a
		trip.addCity e
		trip.addCity b
		trip.addCity c
		trip.addCity d
		def distance = trip.getDistance()
		assertTrue(distance == 22)
		this.generateOutput "4", distance.toString()        
	}
	
	/**
	 * 5. The distance of the route A-E-D.
	 */
	@Test
	public void distanceOfRouteAED() {
		Trip trip = new Trip(railMap)
		trip.addCity a
		trip.addCity e
		trip.addCity d
		def distance = trip.getDistance()
		// TODO
		this.generateOutput "5", "NO SUCH ROUTE"
	}
	
	/**
	 * 6. The number of trips starting at C and ending at C with a maximum of 3
	 * stops.  In the sample data below, there are two such trips: C-D-C (2
	 * stops). and C-E-B-C (3 stops).  
	 */
	@Test
	public void numberOfTripsStartingCEndingC() {
		def delimiter = { trip, city ->
			if(trip.cities.size() < 4) {
				return true
			}
			return false
		}
		List<Trip> tripList = engine.tripsFromToWithDelimiter(c, c, delimiter)
		assertTrue(tripList.size() == 2)
		this.generateOutput "6", tripList.size().toString()
	}
	
	/**
	 * 7. The number of trips starting at A and ending at C with exactly 4 stops.
	 * In the sample data below, there are three such trips: A to C (via B,C,D); A
	 * to C (via D,C,D); and A to C (via D,E,B).  
	 */
	@Test
	public void numberOfTripsStartingAEndingC() {
		def delimiter = { trip, city ->
			if(trip.getNumberOfStops() != 4) {
				return true
			}
			return false
		}
		List<Trip> tripList = engine.tripsFromToWithDelimiter(a, c, delimiter)
		assertTrue(tripList.size() == 3)
		this.generateOutput "7", tripList.size().toString()		
	}
	
	/**
	 * 8. The length of the shortest route (in terms of distance to travel) from A
	 * to C.
	 */
	@Test
	public void lengthOfShortestRouteAToC() {
		int distance = engine.getShortestDistanceFromTo(a,c)
		assertTrue(distance == 9)
		this.generateOutput "8", distance.toString()		
	}
	
	/**
	 * 9. The length of the shortest route (in terms of distance to travel) from B
	 * to B. 
	 */
	@Test
	public void lengthOfShortestRouteBToB() {
		int distance = engine.getShortestDistanceFromTo(b, b)
		assertTrue(distance == 9)
		this.generateOutput "9", distance.toString()
	}
	
	/**
	 * 10. The number of different routes from C to C with a distance of less than
	 * 30.  In the sample data, the trips are: CDC, CEBC, CEBCDC, CDCEBC, CDEBC,
	 * CEBCEBC, CEBCEBCEBC. 
	 */
	@Test
	public void numberOfRoutesFromCToC() {
		def delimiter = { trip, city ->
			if(trip.getDistance(city) <= 30) {
				return true
			}
			return false
		}
		List<Trip> tripList = engine.tripsFromToWithDelimiter(c, c, delimiter)
		assertTrue(tripList.size() == 7)
		this.generateOutput "10", tripList.size().toString() 		
	}
	
	private void generateOutput(String questionNumber, String answerValue) {
		println "Output #$questionNumber: $answerValue"
	}
}
